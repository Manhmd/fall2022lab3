package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt((this.getX() * this.getX()) + (this.getY() * this.getY()) + (this.getZ() * this.getZ()));
    }

    public double dotProduct(Vector3d otherVector){
        
        return ((this.getX() * otherVector.getX()) + (this.getY() * otherVector.getY()) + (this.getZ() * otherVector.getZ());
    } 

    public Vector3d add(Vector3d otherVector){
        Vector3d newVector = new Vector3d((this.getX() + otherVector.getX()), this.getX() + otherVector.getY(), this.getZ() + otherVector.getZ());
        return newVector;
    }
}
